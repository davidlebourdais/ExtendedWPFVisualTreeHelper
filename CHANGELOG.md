# Changelog

## v2.0.0
Initial version for public release, comprising the following visual tree traveling methods:
- FindChild
- FindChildByType
- FindDirectChild
- FindDirectChildByType
- FindAllChildren
- FindAllChildrenByType
- FindParent
- FindParentByType
- FindParentByLevel
- GetParentExtended

[ReadMe Documentation](https://github.com/davidlebourdais/ExtendedWPFVisualTreeHelper/blob/v2.0.0/README.md).
